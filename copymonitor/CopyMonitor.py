"""
This module provides the classes that perform the second stage of the
copy monitor process, the copy itself.
"""
# Copyright (c) 2013, Lior Amar and Gal Oren (liororama@gmail.com | galoren.com@gmail.com)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of Lior Amar or Gal Oren, nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Lior Amar BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import subprocess
import sys
import os
import time
import argparse
import select
import pty
import re

import MetadataScanner


class ICopyMonitor(object):
    """
    The ICopyDirectory interface providing the base class for the copymonitor

    For each program implementing a copy (e.g. cp, rsync) a new class implementing
    this interface should be generated.
    """
    def __init__(self,
                 name,
                 meta_data_scanner,
                 source_dir,
                 destination_dir,
                 progress_obj,
                 verbose=0,
                 timeout = 0.5):
        self.name = name
        self.source_dir = source_dir
        self.destination_dir = destination_dir
        self.verbose = verbose

        self.source_path = self.source_dir
        self.destination_path = self.destination_dir

        self.total_size_of_files = meta_data_scanner.get_total_size()
        self.files_dict = meta_data_scanner.get_files_dict()
        self.prev_size = 0
        self.prev_files = 0
        self.current_size = 0
        self.current_files = 0
        self.already_copied = 0

        self.in_progress_src = None
        self.in_progress_dst = None
        self.in_progress_dst_size = 0

        self.child_done = False
        self.child_pid = -1
        self.child_exit_status = -1

        self.master_fd = None
        self.progress_obj = progress_obj
        (self.update_limit_files, self.update_limit_size) = self.progress_obj.get_copy_update_limits()
        self.timeout = timeout
        self.line_number = 0
        self.partial_line = ""
        self.partial_read_count = 0
        self.current_count_error = 0

        self.error_list = []  # The list will contain tuples of file name and error

    def pre_copy_operation():
        pass

    def create_destination_path(self):

        if not os.path.exists(self.destination_path):
            os.makedirs(self.destination_path)

    def fork_child_in_pty(self, cmd):

        try:
            (child_pid, child_fd) = pty.fork()
        except OSError as e:
            if self.verbose:
                print("fork failed {0}".format(e))
            return (None, None)

        # In child process
        if child_pid == pty.CHILD:
            try:
                os.execlp(cmd[0], *cmd)
            except:
                sys.stdout.write("CP_MONITOR_INTERNAL_ERROR: exec failed [{0}]".format(cmd[0]))
                sys.exit(2)

        # In parent process
        return (child_pid, child_fd)

    def read_lines_from_child(self, child_fd):
        """
        Read lines from child_fd. Return list of lines

        The method read as much as possible from the fd. If the buffer
        read does not end with a newline then the last line (which is
        partial) is saved for future read.
        """

        try:
            size = 65536
            buff = os.read(child_fd, size)
            buff_len = len(buff)

            if self.verbose:
                print "Buff:[", buff, "]"

            half_read = False
            if self.verbose:
                print "\n\n\nBufflen", buff_len
            if (buff_len == size - 1) and (buff[-1] != '\n'):
                if self.verbose:
                    print "Detected possible problem in buffer"
                self.partial_read_count += 1
                half_read = True

            # In case of a previously partial line we preappend it to buff
            if self.partial_line:
                if self.verbose:
                    print "Prepending partial line"
                buff = self.partial_line + buff

            tmp_list = buff.split("\r\n")

            if tmp_list[-1] == "":
                if self.verbose:
                    print "Removing empty part at end of list"
                tmp_list.pop()

            if self.verbose:
                print "Number of lines: {0}\n".format(len(tmp_list))

            if half_read:
                self.partial_line = tmp_list.pop()
                if self.verbose:
                    print "Partial [{0}]".format(self.partial_line)

            if self.verbose:
                print "\n\nGot {0} lines size {1}\n\n".format(len(tmp_list), len(buff))

            #self.line_list.pop()
            return tmp_list
            #line = child_f.readlines()
            #print "LLL", line

        except OSError as e:
            if self.verbose :
                print "Got exception on child_fd probably copy is done"
            (pid, status) = os.waitpid(self.child_pid, os.WNOHANG)
            if os.WIFEXITED(status):
                self.child_done = True
                self.child_exit_status = os.WEXITSTATUS(status)
                if self.verbose:
                    print "Child exited using exit() {0}".format(self.child_exit_status)
            return None

    def handle_output_line(self, line):
        line = line.strip()
        self.line_number += 1
        if self.verbose:
            print "Got line from child {0}: [{1}]".format(self.line_number, line)

        # Checking the first line for possible exec error
        if self.line_number == 1  and line.split(' ')[0] == "CP_MONITOR_INTERNAL_ERROR:":
            err = " ".join(line.split(' ')[1:])
            self.progress_obj.report_error("Error running external command: {0}".format(err))
            return False

        (src_file, status) = self.parse_cmd_output_line(line)
        if self.verbose:
            print "SRC file: [{0}] Status: {1}".format(src_file, status)

        # If src file is empty we do nothing (removed in cp )
        if not src_file:
            return True

        # A previous in progress file exists this means a copy was completed
        if status:

            if self.in_progress_src:

                if self.in_progress_src in self.files_dict:
                    if self.files_dict[self.in_progress_src]['copied'] == True:
                        print "File [{0}] already copied".format(self.in_progress_src)
                        self.already_copied += 1

                    self.files_dict[self.in_progress_src]['copied'] = True

                    size_of_file = self.files_dict[self.in_progress_src]['size']
                    self.current_size += size_of_file
                    self.current_files += 1

                    if self.verbose:
                        print "Files copied: {0}/{1}".format(self.current_files, len(self.files_dict))
                    #if int(self.current_size) % (1 + int(float(self.total_size_of_files) / float(self.progress_obj.get_bar_len()))) == 0:
                    #print int(self.current_size) % (1 + int(float(self.total_size_of_files) / float(bar_len)))

                    if (self.current_files > (self.prev_files + self.update_limit_files))  or \
                    (self.current_size > (self.prev_size + self.update_limit_size)):
                        self.progress_obj.update_copy_progress(self.current_files, self.current_size)
                        self.prev_files = self.current_files
                        self.prev_size = self.current_size

                else:
                    if self.verbose:
                        print "File [{0}] is not in pre scanned dict can not update progress".format(self.in_progress_src)

            self.in_progress_src = src_file
            self.in_progress_dst = self.get_destination_file_path(line)
        else:
            self.in_progress_src = None
            self.in_progress_dst = None
            self.current_count_error += 1

        return True

    def handle_timeout(self):
        if self.verbose :
            print "Timeout on file [{0}]".format(self.in_progress_src)

        if self.in_progress_src in self.files_dict:
            self.in_progress_dst_size = self.get_size_of_in_progress_copy_file()
            size_of_source_file = self.files_dict[self.in_progress_src]['size']
            if self.verbose:
                print "src size: {0}  dest_size: {1}".format(size_of_source_file, self.in_progress_dst_size)

            self.progress_obj.update_copy_progress(
                self.current_files, self.current_size + self.in_progress_dst_size)

    def copy_dir(self):

        #self.create_destination_path()
        self.pre_copy_operation()

        self.progress_obj.set_total_files(len(self.files_dict))
        self.progress_obj.set_total_size_mb(self.total_size_of_files)

        cmd = self.copy_cmd()
        if self.verbose :
            print "Running command {0}".format(cmd)

        (child_pid, child_fd) = self.fork_child_in_pty(cmd)
        if ((not child_pid) or (not child_fd)):
            self.progress_obj.report_error("Error trying spawn child process\n")
            return None

        # Saving the pid of child process
        self.child_pid = child_pid

        #bar_len = self.progress_obj.get_bar_len()
        self.line_number = 0
        line_list = []
        while True:
            #print "Lines", line_list
            if len(line_list) == 0:
                time.sleep(0.01)
                rlist, wlist, elist = select.select([child_fd], [], [], self.timeout)
                if rlist:
                    if self.verbose:
                        print "Copying"
                    line_list = self.read_lines_from_child(child_fd)
                    if not line_list:
                        break
                else:
                    # Got timeout - detecting size of (probably) big file in copy
                    self.handle_timeout()
            else:
                res = self.handle_output_line(line_list.pop(0))
                if not res:
                    return False

        #sys.stdout.write("\n")
        # Checking if the copy finished with all files
        if (len(self.files_dict) - 1) == self.current_files:
            if self.verbose:
                print "Last update all files copied"
            self.current_files +=1
            self.current_size = self.total_size_of_files
            #sys.stdout.write("\r")
            self.progress_obj.update_copy_progress(len(self.files_dict), self.total_size_of_files)

        # With an exit status of 0 the child finished copy all files.
        # I (lior) noticed that in some cases the child exit before we readd all output
        # lines and not all files are counted.
        # The following line is to overcome such situation in case of success
        if self.child_done and self.child_exit_status == 0:
            self.current_size = self.total_size_of_files
            self.progress_obj.done_copy(len(self.files_dict), self.total_size_of_files, self.child_exit_status)

        if self.verbose:
            print "Copied {0}/{1}".format(self.current_files, len(self.files_dict))
            print "Copy done: Already copied: {0}".format(self.already_copied)
            print "Partial read {0}".format(self.partial_read_count)

        if len(self.error_list) > 0:
            return False
        return True

    def add_error(self, file_name, error_type):
        self.error_list.append([file_name, error_type])

    def get_size_of_in_progress_copy_file(self):
        raise NotImplementedError("Should have implemented this")

    def copy_cmd(self):
        raise NotImplementedError("Should have implemented this")

    def parse_cmd_output_line(self, line):
        raise NotImplementedError("Should have implemented this")

    def get_error(self):
        raise NotImplementedError("Should have implemented this")

    def get_destination_file_path(self, line):
        raise NotImplementedError("Should have implemented this")


class CpMonitor(ICopyMonitor):

    @classmethod
    def is_copy_monitor_for(cls, name):
        return name == "cp"

    def pre_copy_operation(self):
        """
        Fixing source_dir and destination_dir to end without /
        """

        if self.source_dir[-1] == "/":
            self.source_dir = self.source_dir[0:-1]
        if self.destination_dir[-1] == "/":
            self.destination_dir = self.destination_dir[0:-1]

    def copy_cmd(self):
        #return ("./pty_emu.py cp" + " -rv " + self.source_path + " " + self.destination_path)
        #return ([self.current_dir + "/" + 'pty_emu.py cp', '-rv', self.source_path, self.destination_path])
        return (['cp', '-rv', self.source_path, self.destination_path])

    def parse_cmd_output_line(self, line):
        """
        Parsing a line produced by cp. Return the source file

        If the line is in the form [src -> dst] the src is return
        any other for will return an empty source file
        """
        cp_parts = line.split(' ')
        if len(cp_parts) == 3:
            if cp_parts[1] == '->':
                src_file = cp_parts[0][1:-1]
                return (src_file, True)

        if line.startswith("cp: cannot "):
            m = re.match(".*`(.*)'.*", line)
            file_name = "not-detected"
            if m:
                #print "Error:" + m.group(1)
                file_name = m.group(1)
            if self.verbose:
                print "Detected error in cp"
            self.add_error(file_name, line)
            return (file_name, False)

        return ("", True)

    def get_error(self):
        return "using cp"

    def get_destination_file_path(self, line):
        cp_parts = line.split(' ')
        dst_file = cp_parts[2][1:-1]
        return dst_file

    def get_size_of_in_progress_copy_file(self):
        if self.in_progress_dst != '':
            if self.verbose:
                print "In cp getting file [{0}] size".format(self.in_progress_dst)
            #return (os.path.getsize(self.in_progress_dst) / (1024 * 1024))
            if os.path.exists(self.in_progress_dst):
                return (os.lstat(self.in_progress_dst).st_size) / (1024 * 1024)
            else:
                return 0


class RsyncMonitor(ICopyMonitor):
    @classmethod
    def is_copy_monitor_for(cls, name):
        return name == "rsync"

    def pre_copy_operation(self):
        """
        Fixing source_dir and destination_dir to end without /
        """

        if self.source_dir[-1] != "/":
            print "Detected no / at end "
            self.source_dir = self.source_dir + "/"
        if self.destination_dir[-1] != "/":
            self.destination_dir = self.destination_dir + "/"

    def copy_cmd(self):
            return (["rsync", "-rva", "--inplace", self.source_path, self.destination_path])

    def parse_cmd_output_line(self, line):
        rsync_parts = line.split(' ')

        part_path_file = rsync_parts[0]

        #last_dir = self.source_path.split("/")[-1]
        source_path_no_last_dir = "/".join(self.source_path.split("/")[0:-1])
        #print "Source path no last dir [{0}]".format(source_path_no_last_dir)
        #print "Rsync output partial file: [{0}]".format(part_path_file)
        src_file = source_path_no_last_dir + "/" + rsync_parts[0]

        if len(rsync_parts) != 1 or rsync_parts[0] == '\n':
            if line.startswith("rsync: "):
                file_name = re.findall(r'\"(.+?)\"', line)
                #self.add_error("",line)
                if self.verbose:
                    print "Detected error in rsync"
                self.add_error(file_name[0], line)
                return (file_name, False)

        if rsync_parts[0] == 'skipping':
            sys.stdout.write(line)
            return (src_file, True)
        else:
            return (src_file, True)

    def get_error(self):
        return "using rsync"

    def get_destination_file_path(self, line):
        rsync_parts = line.split(' ')
        part_path_file = rsync_parts[0]
        file_parts = part_path_file.split("/")
        if len(file_parts) > 1:
            file_parts.pop(0)

        #print "DDD after pop [{0}]".format(file_parts)
        dst_file = self.destination_path + "/" + "/".join(file_parts)

        #print "DEST [{0}]".format(dst_file)
        return dst_file

    def get_size_of_in_progress_copy_file(self):
        if self.in_progress_dst != '':
            if self.verbose:
                print "In rsync getting file [{0}] size".format(self.in_progress_dst)

        try:
            dst_size = os.lstat(self.in_progress_dst).st_size
        except OSError:
            return 0
        return (dst_size) / (1024 * 1024)


class TarMonitor(ICopyMonitor):

    # tar cf - d1 | tar xvfp -  -C d2 --strip-components=1

    @classmethod
    def is_copy_monitor_for(cls, name):
        return name == "tar"

    def pre_copy_operation(self):
        """
        Fixing source_dir and destination_dir to end without /
        """

        if self.source_dir[-1] != "/":
            print "Detected no / at end "
            self.source_dir = self.source_dir + "/"
        if self.destination_dir[-1] != "/":
            self.destination_dir = self.destination_dir + "/"
        if not os.path.exists(self.destination_dir):
            os.makedirs(self.destination_dir)

    def copy_cmd(self):
        return (["tar", " cf - ", self.source_path, " | ",
             "tar", " xvfp - ", " -C ", self.destination_path, "--strip-components 1"])
        #return ("tar" + " cf - " + self.source_path + "/*" + " | (cd " + self.destination_path + " && tar xBf -)")

    def parse_cmd_output_line(self, line):
        tar_parts = line.split(' ')

        part_path_file = tar_parts[0]
        src_file = part_path_file

        if len(tar_parts) != 1 or tar_parts[0] == '\n':
            if line.startswith("tar: "):
                file_name = re.findall(r'\"(.+?)\"', line)
                #self.add_error("",line)
                if self.verbose:
                    print "Detected error in tar"
                self.add_error(file_name[0], line)
                return (file_name, False)

        if tar_parts[0] == 'skipping':
            sys.stdout.write(line)
            return (src_file, True)
        else:
            return (src_file, True)

    def get_error(self):
        return "using tar"

    def get_destination_file_path(self, line):

        tar_parts = line.split(' ')
        part_path_file = tar_parts[0]
        original_file_name = "/".join(a.split('/')[1:])
        dst_file = self.destination_path + "/" + original_file_name

        return dst_file

    def get_size_of_in_progress_copy_file(self):
        if self.in_progress_dst != '':
            if self.verbose:
                print "In tar getting file [{0}] size".format(self.in_progress_dst)

        try:
            dst_size = os.lstat(self.in_progress_dst).st_size
        except OSError:
            return 0
        return (dst_size) / (1024 * 1024)


def CopyMonitorFactory(name, mds, source_dir, dest_dir, tpb, verbose):
    #print "In factory:", name
    for cls in ICopyMonitor.__subclasses__():
        if cls.is_copy_monitor_for(name):
            return cls(name, mds, source_dir, dest_dir, tpb, verbose)
    raise ValueError
