"""
This module provides the MetadataScanner object
"""
# Copyright (c) 2013, Lior Amar and Gal Oren (liororama@gmail.com | galoren.com@gmail.com)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of Lior Amar or Gal Oren, nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Lior Amar BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import subprocess
import sys
import os
import time
import argparse
import select
import pty
import re


class MetadataScanner(object):
    """
    Scan a directory (recursively) a collect information about all files

    This class perform the first stage of the monitor copy process.The
    collected will be used to produce the progress information of the second
    part (the copy itself)
    """

    def __init__(self, dir_to_scan, progress_obj, verbose=0):

        self.dir_to_scan = dir_to_scan
        self.total_size_mb = 0
        self.dir_count = 0
        self.files_dict = {}
        self.progress_obj = progress_obj

    def scan(self, update_count = 100):
        """
        Recursively scan a directory collect information about each file, return True or False

        Return True on successful scan
        Return False on bad scan
        """
        beginning_time = time.time()

        walk_dir = self.dir_to_scan
        for root, sub_dirs, files in os.walk(walk_dir):
            self.dir_count += len(sub_dirs)
            for file in files:
                file_name = os.path.join(root, file)
                file_size = os.lstat(file_name).st_size

                self.files_dict[file_name] = {"size": file_size / (1024.0 * 1024.0), "copied": False}
                self.total_size_mb += file_size / (1024.0 * 1024.0)

                if len(self.files_dict) % update_count == 0:
                    current_time = time.time()
                    if current_time - beginning_time > 0.05:
                        self.progress_obj.update_scan_progress(len(self.files_dict), self.total_size_mb)
                        beginning_time = time.time()

        # finalizing scan files progress bar
        self.progress_obj.update_scan_progress(len(self.files_dict), self.total_size_mb)
        # End of scan setting final number in the progress obj
        self.progress_obj.set_total_files(len(self.files_dict))
        self.progress_obj.set_total_size_mb(self.total_size_mb)

        return True

    def get_total_size(self):
        return self.total_size_mb

    def get_total_files(self):
        return len(self.files_dict)

    def get_files_dict(self):
        return self.files_dict

    def get_file_info(self, file_name):
        if file_name in self.files_dict:
            print (self.files_dict[file_name])
            return self.files_dict[file_name]
        return None

    def get_dir_count(self):
        return self.dir_count

    def print_dictionary(self):
        print("")
        i=0
        for file_name in self.files_dict:
            print("{0} {1} : {2} MB".format(i, file_name, self.files_dict[file_name]['size']))
            i += 1
