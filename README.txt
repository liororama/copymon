Copymon - monitor long copy processes
=======================================

Authors: Lior Amar, Gal Oren

License: BSD Revised License



Introduction:
=============
Copymon package provides means to monitor a copy operation
(of directories and files) and present a progress bar during
the operation.

Each copy operation is divieded into two main stages:

* Metadata scan stage, which generate information about the files
  to be copied.
* The copy itself. This package does not implement a the copy operation but
  rather uses external programs like rsync,cp and tar to perform the actual copy.


Installation Notes:
===================
If you downloaded the source of copymon just do the following:

Using PIP:
----------
cd copymon
python setup.py sdist
pip install dist/copymon-0.1.0.tar.gz

Direct install:
---------------
cd copymon
python setup.py install


The install process will install the program copymon.py in /usr/local/bin

copymon Examples:
-----------------

* Copy of a directory using cp as the copy command
 copymon.py --cp /usr/share/doc /tmp/doc

* Running in scan only mode:
 copymon.py --only-scan --cp /usr/share/doc /tmp/doc


Using copymon:
==================
copymon.py --help
