#! /usr/bin/env python
# Copyright (c) 2013, Lior Amar amd Gal Oren (liororama@gmail.com)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of Lior Amar or Gal Oren, nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Lior Amar BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


'''
    The cpwpb.py program copy files from one directory to another directory using cp, rsync and tar commands
        while presenting the progress evaluation of the whole copy progress (from 0% to 100%).
    The program also gets the metadata of the direcotry and present it.
'''
# Copyright (c) 2013, Lior Amar and Gal Oren (liororama@gmail.com | galoren.com@gmail.com)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of Lior Amar or Gal Oren, nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Lior Amar BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys
import os
import argparse

from copymonitor import *

description_help = """
copymon monitor a copy operation and presents a progress bar

Usages:

Copy using cp:    ./copymon.py --cp ~/src/d1/ ~/src/d2/
Copy using rsync: ./copymon.py --rsync ~/src/d1/ ~/src/d2/
Copy with Colors: ./copymon.py -p color-terminal --cp - ~/src/d1/ ~/src/d2/

"""

authors_help = """
Authors: Lior Amar, Gal Oren
"""


def main():

    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                     description=description_help, epilog=authors_help)

    parser.add_argument("source_dir", type=str, metavar="source-dir", help="Source directory")
    parser.add_argument("destination_dir", type=str, metavar="destination-dir", help="Destination directory")

    parser.add_argument("--only-scan", "-s", action="store_true",
                        dest="scan_only", default=False, help='Perform only scan, omit copy stage')

    parser.add_argument('--cp', action='store_const', dest='cp_command',
                        const='cp',
                        help='Copy directory using cp')
    parser.add_argument('--rsync', action='store_const', dest='cp_command',
                        const='rsync',
                        help='Copy directory using rsync')
    parser.add_argument('--tar', action='store_const', dest='cp_command',
                        const='tar',
                        help='Copy directory using tar')

    parser.add_argument('-p', '--progress', default="terminal", dest="progress_bar_type",
                help="Progress bar type (terminal, color-terminal)", metavar="TYPE")

    parser.add_argument('--verbose', action='store_const', dest='verbose',
                        const='value-to-store',
                        help='Verbose')

    parser.add_argument('--error', action='store_const', dest='error',
                        const='value-to-store',
                        help='Error')

    options = parser.parse_args()
    if options.cp_command == None:
        print "Please specify type of copy program to use (see help using --help)"
        sys.exit(1)

    # Fixing source_dir and destination_dir to end without /
    # /etc /tmp/etc
    #if options.source_dir[-1] == "/":
    #   print "Removing / at end"
    #   options.source_dir = options.source_dir[0:-1]
    #if options.destination_dir[-1] == "/":
    #   print "Removing / at end"
    #   options.destination_dir = options.destination_dir[0:-1]
    #

    if options.verbose:
        print "CMD:   ", options.cp_command
        print "Source:", options.source_dir
        print "Dest:  ", options.destination_dir

    update_count = 1

    # Creating the terminal based progress object
    tpb = ProgressBar.ProgressBarFactory(options.progress_bar_type, bar_len=30)

    # Performing the metadata scan
    metadata_scanner = MetadataScanner.MetadataScanner(options.source_dir, progress_obj=tpb, verbose=options.verbose)
    res = metadata_scanner.scan(update_count)
    if not res:
        if options.verbose:
            print "Aborting copy due to errors in scan"
        sys.exit(1)

    if options.verbose:
        print("\n-----------------------------------------------------")
        print("Tota Size:          {0:.2f} MB".format(metadata_scanner.get_total_size()))
        print("Files Number:       {0}".format(len(metadata_scanner.get_files_dict())))
        print("Sub-Folders Number: {0}".format(metadata_scanner.get_dir_count()))
        print("-----------------------------------------------------\n")

    # Providing scan_only mode
    if options.scan_only:
        if options.verbose:
            print "Skipping copy - scan_only mode"
        sys.exit(0)

    # Creating the specific copy monitor object.
    copy_obj = CopyMonitor.CopyMonitorFactory(options.cp_command,
                        metadata_scanner,
                        options.source_dir,
                        options.destination_dir,
                        tpb,
                        options.verbose)
    if options.verbose:
        print "Before copy"

    # Setting general (from base class) copy object properties
    #copy_obj.set_verbose(options.verbose)
    #copy_obj.set_dir_metadata(directory_meta_data)
    #copy_obj.set_source_dir(options.source_dir)
    #copy_obj.set_dest_dir(options.destination_dir)

    # Running the copy
    res = copy_obj.copy_dir()
    sys.stdout.write("\n")

    if options.error:
        if not res:
            sys.stdout.write("\n")
            if options.verbose:
                sys.stdout.write ("\nError in copy")
            line_number = 1
            for error in copy_obj.error_list:
                print ("({0}) File: {1}  \n  | {2}".format(line_number, error[0],error[1]))
                line_number = line_number + 1
            sys.exit(1)
    print " "


if __name__ == "__main__":
    main()
